-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.19 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table giftano.sys_kategori
CREATE TABLE IF NOT EXISTS `sys_kategori` (
  `kategori_id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori_name` varchar(100) DEFAULT NULL,
  `kategori_datetime` datetime DEFAULT NULL,
  `kategori_was_deleted` enum('Y','N') DEFAULT 'N',
  PRIMARY KEY (`kategori_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table giftano.sys_kategori: 4 rows
/*!40000 ALTER TABLE `sys_kategori` DISABLE KEYS */;
INSERT IGNORE INTO `sys_kategori` (`kategori_id`, `kategori_name`, `kategori_datetime`, `kategori_was_deleted`) VALUES
	(1, 'kategori a', '2019-07-28 16:48:52', 'Y'),
	(2, 'kategori b', '2019-07-28 16:49:08', 'N'),
	(3, 'kategori c', '2019-07-28 16:51:10', 'N'),
	(4, 'kategori d', '2019-07-28 16:57:56', 'N');
/*!40000 ALTER TABLE `sys_kategori` ENABLE KEYS */;

-- Dumping structure for table giftano.sys_produk
CREATE TABLE IF NOT EXISTS `sys_produk` (
  `produk_id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori_id` int(11) NOT NULL DEFAULT '0',
  `produk_name` text,
  `produk_price` decimal(10,0) DEFAULT NULL,
  `produk_deskripsi` text,
  `produk_datetime` datetime DEFAULT NULL,
  `produk_was_deleted` enum('Y','N') DEFAULT 'N',
  PRIMARY KEY (`produk_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table giftano.sys_produk: 0 rows
/*!40000 ALTER TABLE `sys_produk` DISABLE KEYS */;
INSERT IGNORE INTO `sys_produk` (`produk_id`, `kategori_id`, `produk_name`, `produk_price`, `produk_deskripsi`, `produk_datetime`, `produk_was_deleted`) VALUES
	(1, 1, 'produk f', 12000, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2019-07-29 15:53:29', 'N'),
	(2, 1, 'produk b', 12000, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2019-07-29 12:41:06', 'N'),
	(3, 1, 'produk c', 12000, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2019-07-29 12:41:20', 'N'),
	(4, 2, 'produk d', 12000, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2019-07-29 12:42:10', 'N');
/*!40000 ALTER TABLE `sys_produk` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
