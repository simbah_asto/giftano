<?php 
$string = "<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Build by generator taraCode
 *
 *
 * @package Controler
 * @author Asto
 */

class ".$classname." extends CI_Controller {

	private \$mainModel;
	private \$modul='".$this->controler."';
	private \$page='".$this->modul."';
	private \$data = array();
	private \$core_template;

	function __construct()
	{
		parent::__construct();
		\$this->core_template = \$this->config->item('core_template');

	}

	
	public function index()
	{
		// load navbar
		\$this->data['navbar'] = \$this->core_template.'/'.'component/'.'navbar_'.\$this->modul;
		// load footer
		\$this->data['sidebar'] = \$this->core_template.'/'.'component/'.'sidebar_'.\$this->modul;
		template(\$this->modul, \$this->page, \$this->data, __FUNCTION__);
	}

	// function get ui input
	public function get_form_input()
	{

	}

	// function get ui edit
	public function get_form_edit()
	{

	}

}

/* End of file Controler */
/* Generate by TaraCode */";

?>