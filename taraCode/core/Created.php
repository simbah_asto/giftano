<?php 

/**
 * summary
 */

require_once 'Helper.php';
class Created 
{
	private $controler;
	private $view;
	private $model;
	private $modul;
	private $suffix = '_lib.php';
	private $core_template = 'one';
	private $table;

	function __construct($controler = '', $modul = '', $model = '', $table)
	{
		$this->controler = $controler;
		$this->model = $model;
		$this->modul = $modul;
		$this->table = $table;
	}

	public function Created_controler()
	{
		$json = readJson('core/setting.json');
		$controler = $json->target.'controllers/'.$this->modul.'/'.ucwords($this->controler).'.php';
		if (file_checking($this->controler)) {
			$classname = ucwords($this->controler);
			include 'Controller.php';
			file_create($controler, $string);
			return array('message' => 'Success, Controler has created!', 'status' => 200);
		}

		return array('message' => 'Error, Controler has been created!', 'status' => 200);

	}

	public function Created_model()
	{
		$json = readJson('core/setting.json');
		$models = $json->target.'models/'.$this->modul.'/'.ucwords($this->model).$this->suffix;
		if (file_checking($this->models)) {
			$classname = ucwords($this->model).'_lib';
			include 'Model.php';
			file_create($models, $string);
			return array('message' => 'Success, Models has created!', 'status' => 200);
		}

		return array('message' => 'Error, Models has created!', 'status' => 200);
	}

	public function Created_module()
	{
		$json = readJson('core/setting.json');
		$controler = $json->target.'controllers';
		$models = $json->target.'models';
		$view = $json->target.'views'.'/'.$this->core_template;
		$view_modul = $json->target.'views'.'/'.$this->core_template.'/'.$this->controler;
		$view_index = $view_modul.'/'.'index.php';
		if (dir_checking($controler) AND dir_checking($models) AND dir_checking($view)) {
			create_dir($controler,$this->modul);
			create_dir($models, $this->modul);
			create_dir($view, $this->controler);
			create_dir($view_modul, $this->modul);
			file_create($view_index, "as");
			return array('message' => 'Success, Modul on controler and model is created!', 'status' => 200);
		}
		return array('message' => $models, 'status' => 500);
	}

	public function Created_views()
	{
		$json = readJson('core/setting.json');
		$view = $json->target.'views'.'/'.$this->core_template.'/'.$this->controler;
		$view_index = $view.'/'.$this->modul.'/'.'/index.php';
		$view_input = $view.'/'.$this->modul.'/'.'get_form_input.php';
		$view_edit = $view.'/'.$this->modul.'/'.'get_form_edit.php';

		if (dir_checking($view)) {
			create_dir($view, $this->modul);
			file_create($view_index, "as");
			file_create($view_input, "as");
			file_create($view_edit, "as");
			return array('message' => 'Your view has created!', 'status' => 200);
		}
		return array('message' => 'Error, Your directory modul on your view is not falid', 'status' => 500);
	}

	public function close_created()
	{
		$this->controler = null;
		$this->model = null;
		$this->modul = null;
		$this->table = null;
		$dataPost = array();
	}
}
?>
