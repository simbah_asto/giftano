<?php 
require_once 'core/Helper.php';
require 'Created.php';

$dataPost = (isset($_POST))?$_POST:array();
$response = array();

if (isset($dataPost['submit'])) {
	$modelName = text($dataPost['model']);
	$modulName = text($dataPost['modul']);
	$controler = text($dataPost['controler']);
	$table = text($dataPost['table']);
	// $response = array();

	if (is_null($modulName) OR $modulName == "") {
		$response[] = array('message' => 'Your <b>Module</b> name is not valid', 'status'=> 500);
		return $response;
	}
	if (is_null($modelName) OR $modelName == "") {
		$response[] = array('message' => 'Your <b>Model</b> name is not valid', 'status'=> 500);
		return $response;
	}

	if (is_null($controler) OR $controler == "") {
		$response[] = array('message' => 'Your <b>Controler or File</b> name is not valid', 'status'=> 500);
		return $response;
	}

	if (is_null($table) OR $table == "") {
		$response = array('message' => 'Your <b>Table</b> name is not valid', 'status'=> 500);
		return $response;
	}
	$created = new Created($controler, $modulName, $modelName, $table);

	// modul created
	$modul = $created->Created_module();
	if ($modul['status'] !== 200) {
		$response[] = array('message' => 'Failed modul Created', 'status' => 500);
		return $response;
	}
	$response[] = $modul;
	// create controler

	$control = $created->Created_controler();
	if ($control['status'] !== 200) {
		$response[] = array('message' => 'Failed controler Created', 'status' => 500);
		return $response;
	}
	$response[] = $control; 
	// create controler
	$model = $created->Created_model();
	if ($model['status'] !== 200) {
		$response[] = array('message' => 'Error, Failed models Created', 'status' => 500);
		return $response;
	}
	$response[] = $model;
	
	$views = $created->Created_views();
	if ($views['status'] !== 200) {
		$response[] = array('message' => 'Failed View Created', 'status' => 500);
		return $response;
	}
	$clear = $created->close_created();
	$response[] = $views;

	// return $response;
	// $response = array('message' => 'Success', 'status' => 200);
	// return $r;


}




?>

