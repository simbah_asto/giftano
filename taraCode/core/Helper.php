<?php 

function file_create($path, $string)
{
	if (file_checking($path)) {
		$create = fopen($path, "w") or die("Change your permision folder for application and harviacode folder to 777");
		fwrite($create, $string);
		fclose($create);
	}

	return $path;
}

function file_checking($path = '')
{
	if (!file_exists($path)) {
		return true;
	}
	return false;
}

function dir_checking($path='')
{
	if (is_dir($path)) {
		return true;
	}
	return false;
}

function create_dir($path='', $file='')
{
	$folder = $path.'/'.$file;

	mkdir($folder, 0777, true);
}

function readJson($path='')
{
	$json = file_get_contents($path);
	$json = json_decode($json);
	return $json;

}

function text($value='')
{
	$string = strip_tags(trim($value));
	return $string;
}

?>