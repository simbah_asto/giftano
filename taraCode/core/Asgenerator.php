<?php 
/**
 * by: Asto Nugroho
 */
class Asgenerator 
{
	private $host;
	private $user;
	private $sql;
	private $database;
	private $password;

	function __construct()
	{

		$this->connection();
	}


	public function connection()
	{
		// get database file on framwork
		$subject = file_get_contents('../application/config/database.php');
		// get isi file database 
		$string = str_replace("defined('BASEPATH') OR exit('No direct script access allowed');", "", $subject);

		$con = 'core/connection.php';
		$create = fopen($con, "w") or die("Change your permision folder for application and harviacode folder to 777");
		fwrite($create, $string);
		fclose($create);
		require $con;

		$this->host = $db['default']['hostname'];
		$this->user = $db['default']['username'];
		$this->password = $db['default']['password'];
		$this->database = $db['default']['database'];

		$this->sql = new mysqli($this->host, $this->user, $this->password, $this->database);
		if ($this->sql->connect_error)
		{
			echo $this->sql->connect_error . ", please check 'application/config/database.php'.";
			die();
		}
		unlink($con);
	}

	public function get_table()
	{
		$query = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA=?";
		$stmt = $this->sql->prepare($query) OR die("Error code :" . $this->sql->errno . " (not_primary_field)");
		$stmt->bind_param('s', $this->database);
		$stmt->bind_result($table_name);
		$stmt->execute();
		while ($stmt->fetch()) {
			$fields[] = array('table_name' => $table_name);
		}
		return $fields;
		$stmt->close();
		$this->sql->close();
	}
}

$generator = new Asgenerator();
?>