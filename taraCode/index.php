<?php 
error_reporting(0);
require_once 'core/Asgenerator.php';
require_once 'core/Execute.php';
?>

<!DOCTYPE html>
<html>
<head>
	<title>Astara Generator</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script> 

	<link href="https://fonts.googleapis.com/css?family=Duru+Sans" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="assets/css/main.css">

</head>
<body class="pt-5" style="background: #F2F2F2;">
	<div class="container w-75 m-auto bg-white shadow-sm rounded p-4 border mb-4">
		<div class="row mb-3">
			<div class="col-md-12">
				<h2 class="text-center ">Astara GENERATOR</h2>
			</div>

		</div>
		<form action="" method="POST" class="row w-75 mr-auto d-sm-flex" style="margin: auto;">
			<div class="col-md-6 d-flex ">
				<div class="form-group w-100">
					<label for="file" class="text-primary">File Name</label>
					<input type="text" name="controler" class="form-control" placeholder="File Controler Name">
				</div>
			</div>
			<div class="col-md-6 d-flex ">
				<div class="form-group w-100">
					<label for="file" class="text-primary">Modul Name</label>
					<input type="text" name="modul" class="form-control" placeholder="Folder Module Name">
				</div>

			</div>
			<div class="col-md-6 d-flex ">
				<div class="form-group w-100">
					<label for="file" class="text-primary">Model Name</label>
					<input type="text" name="model" class="form-control" placeholder="Folder Module Name">
				</div>

			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label for="" class="text-primary">Table Name</label>
					<select name="table" class="form-control">
						<option value="" selected>Table Name</option>
						<option value="no table" selected>do not use table </option>
						<?php 

						$table = $generator->get_table();
						foreach ($table as $value) {?>
							<option value="<?php echo $value['table_name'] ?>"><?php echo $value['table_name'] ?></option>
							
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="col-md-6 mb-4">
				<label class="text-primary">You will Create To:</label>
				<div class="custom-control custom-switch">
					<input type="radio" class="custom-control-input" id="all" name="action" value="0" checked>
					<label class="custom-control-label" for="all">ALL</label>
				</div>
				<div class="custom-control custom-switch">
					<input type="radio" class="custom-control-input" id="control" name="action" value="1">
					<label class="custom-control-label" for="control">Controlers</label>
				</div>
				<div class="custom-control custom-switch">
					<input type="radio" class="custom-control-input" id="model" name="action" value="2">
					<label class="custom-control-label" for="model">Models</label>
				</div>
				<div class="custom-control custom-switch">
					<input type="radio" class="custom-control-input" id="view" name="action" value="3">
					<label class="custom-control-label" for="view">Views</label>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label for=""></label>
					<input type="submit" name="submit" class="btn btn-success" value="Generate">
				</div>
				<hr>
				<?php if (!empty($response)) {?>
					<label>Execute Process:</label>
					<?php foreach ($response as $value) { ?>
						<?php if ($value['status'] == 200) {?>
							<div class="alert alert-success">
								<?php echo $value['message'] ?>
							</div>
						<?php }else{ ?>
							<div class="alert alert-danger">
								<?php echo $value['message'] ?>
							</div>
						<?php } ?>
					<?php } ?>
				<?php } ?>
			</div>

			<div class="col-md-12">
				<h5>Deskripsi Varible</h5>
				<div class="alert alert-primary">
					<ul>
						<li><b>File Name </b> Adalah Stackholder atau User pengguna yang berada dalam folder controler</li>
						<li><b>Modul Name</b> Adalah nama Folder dalam Controler dan model yang diperuntukan untuk pemanaam model dan view</li>
						<li><b>Model Name</b> Adalah nama Model yang nantinya di peruntukan untuk nama file Model</li>
						<li><b>Table Name</b> Adalah nama table yang akan dirujuk untuk diakses pada modul yang telah tergenerate</li>

					</ul>
				</div>
				<h5>Astara Generator V.1.0</h5>
				<div class="alert alert-primary">
					<ul>
						<li>Astara generator V.1.0 Adlah aplikasi generator CRUD versi 1.0 dalam versi ini merupakan generete hanya dapat membuatkan file controler dan file model serta penamaan modul atau folder dalam controler</li>
					</ul>
				</div>
			</div>
		</div>
	</form>
	<footer class="mt-5 shadow-sm border bg-white ">
		<p class="mt-2 mb-2 text-center">Created by Asto Nugroho. Updated on 30 april 2019</p>
	</footer>
</body>
</html>

