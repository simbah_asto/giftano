<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Validation
{
	protected $ci;

	public function __construct()
	{
		$this->ci =& get_instance();
	}


	// ceking variable input yang tidak boleh kosong
	public function validation_($validation = array())
	{
		$postData = isset($_POST)?$_POST:array();
		$postKey = array_keys($validation);
		if (!empty($postData) AND !empty($validation)) {
			for ($i = 0; $i < count($postKey) ; $i++) {
				$postValue = $this->ci->input->post($postKey[$i]);
				if (array_key_exists($postKey[$i], $postData)) {
					if (trim($postValue) == '' || is_null(stripcslashes(trim($postValue)))) {
						return array('message' => $validation[$postKey[$i]], 'status' => 500 );
					}
				}else{
					return array('message' => $validation[$postKey[$i]], 'status' => 500);
				}
			}
			return array('message' => 'data received ', 'status' => 200 );

		}

		return array('message' => 'tidak ada data yang di kirim', 'status' => 500);
	}

	public function validate_email($value)
	{
		$email = input($value);
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			return array('message' => 'Email format', 
				'status' => 500
			);
		}
	}

	public function input($value='')
	{
		$data = trim($value);
		$data = stripcslashes($value);
		$data = htmlspecialchars($value);
		return $data;
	}



}

/* End of file Validation.php */
/* Location: ./application/libraries/Validation.php */
