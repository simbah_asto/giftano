<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'core/core_api.php';

class Product extends Core_api {

	
	private $mainModel;
	private $modul='admin';
	private $page='product';
	private $data = array();
	private $core_template;
	private $mainKategori;

	function __construct()
	{
		parent::__construct();
		$this->core_template = $this->config->item('core_template');
		$this->load->model(array('product/product_lib', 'kategori/kategori_lib'));
		$this->mainModel = new product_lib;
		$this->mainKategori = new kategori_lib;
	}

	public function set_produk()
	{
		$validation =array('harga' => 'harga cannot be empty', 'name' => 'product name cannot be empty', 'deskripsi' => 'deskripsi cannot be empty', 'kategori' => 'kategori cannot be empty');	
		$response = $this->validation->validation_($validation);
		if ($response['status'] == 200) {
			$response = $this->mainModel->do_insert_data();
			echo json_encode($response);
		}else{
			echo json_encode($response);
		}
	}

	public function rest_load_data()
	{
		$response = $this->mainModel->do_rest_load_data();
		echo json_encode($response);
	}


	public function update_produk()
	{
		$validation =array('harga' => 'harga cannot be empty', 'name' => 'product name cannot be empty', 'deskripsi' => 'deskripsi cannot be empty', 'kategori' => 'kategori cannot be empty', 'id' => 'id produk cannot be empty');	
		$response = $this->validation->validation_($validation);
		if ($response['status'] == 200) {
			$response = $this->mainModel->do_updated_data();
			echo json_encode($response);
		}else{
			echo json_encode($response);
		}
	}

	public function deleted()
	{
		$validation =array( 'id' => 'id produk cannot be empty');	
		$response = $this->validation->validation_($validation);
		if ($response['status'] == 200) {
			$response = $this->mainModel->do_deleted_data();
			echo json_encode($response);
		}else{
			echo json_encode($response);
		}
	}

}

/* End of file Api.php */
/* Location: ./application/controllers/api/Api.php */