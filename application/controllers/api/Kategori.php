<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'core/core_api.php';

class Kategori extends Core_api {
	
	private $mainModel;
	private $modul='admin';
	private $page='kategori';
	private $data = array();
	private $core_template;

	function __construct()
	{
		parent::__construct();
		$this->core_template = $this->config->item('core_template');
		$this->load->model(array('kategori/kategori_lib'));
		$this->mainModel = new kategori_lib;
	}

	
	public function set_kategori()
	{
		$validation = array('kategori' => 'kategori cannot be empty!');
		$response = $this->validation->validation_($validation);
		if ($response['status'] == 200) {
			$response = $this->mainModel->do_insert_data();
			echo json_encode($response);
		}else{
			echo json_encode($response);
		}
	}

	public function update()
	{
		$validation = array('id' => 'Your id is not found', 'kategori' => 'Your kategori cannot be empty');
		$response = $this->validation->validation_($validation);

		if ($response['status'] == 200) {
			$response = $this->mainModel->do_updated_data();
			echo json_encode($response);
		}else{
			echo json_encode($response);
		}
	}

	public function deleted()
	{
		$validation = array('id' => 'your id is not found');
		$response = $this->validation->validation_($validation);
		if ($response['status'] == 200) {
			$response = $this->mainModel->do_deleted_data();
			echo json_encode($response);
		}else{
			echo json_encode($response);
		}
	}

	public function rest_load_data()
	{
		$response = $this->mainModel->do_rest_load_data();
		echo json_encode($response);
	}
}

/* End of file Kategori.php */
/* Location: ./application/controllers/api/Kategori.php */