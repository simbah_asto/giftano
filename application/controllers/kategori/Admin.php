<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Build by generator taraCode
 *
 *
 * @package Controler
 * @author Asto
 */

class Admin extends CI_Controller {

	private $mainModel;
	private $modul='admin';
	private $page='kategori';
	private $data = array();
	private $core_template;

	function __construct()
	{
		parent::__construct();
		$this->core_template = $this->config->item('core_template');
		$this->load->model(array('kategori/kategori_lib'));
		$this->mainModel = new kategori_lib;
	}

	
	public function index()
	{
		// load navbar
		$this->data['navbar'] = $this->core_template.'/'.'component/'.'navbar_'.$this->modul;
		// load footer
		$this->data['sidebar'] = $this->core_template.'/'.'component/'.'sidebar_'.$this->modul;
		template($this->modul, $this->page, $this->data, __FUNCTION__);
	}

	// function get ui input
	public function get_form_input()
	{
		$this->load->view($this->core_template.'/'.$this->modul.'/'.$this->page.'/'.__FUNCTION__);

	}

	// function get ui edit
	public function get_form_edit()
	{
		$validation = array('id' => 'your id is not found');
		$response = $this->validation->validation_($validation);
		if ($response['status'] == 200) {
			$this->data['response'] = $this->mainModel->do_get_data_by_id();
			$this->load->view($this->core_template.'/'.$this->modul.'/'.$this->page.'/'.__FUNCTION__, $this->data);
		}else{
			$this->load->view($this->core_template.'/'.$this->modul.'/'.$this->page.'/'.__FUNCTION__, $this->data);
		}

	}


	public function set_kategori()
	{
		$validation = array('kategori' => 'kategori cannot be empty!');
		$response = $this->validation->validation_($validation);
		if ($response['status'] == 200) {
			$response = $this->mainModel->do_insert_data();
			echo json_encode($response);
		}else{
			echo json_encode($response);
		}
	}

	public function update()
	{
		$validation = array('id' => 'Your id is not found', 'kategori' => 'Your kategori cannot be empty');
		$response = $this->validation->validation_($validation);

		if ($response['status'] == 200) {
			$response = $this->mainModel->do_updated_data();
			echo json_encode($response);
		}else{
			echo json_encode($response);
		}
	}

	public function deleted()
	{
		$validation = array('id' => 'your id is not found');
		$response = $this->validation->validation_($validation);
		if ($response['status'] == 200) {
			$response = $this->mainModel->do_deleted_data();
			echo json_encode($response);
		}else{
			echo json_encode($response);
		}
	}

	public function do_rest_load_data()
	{
		$response = $this->mainModel->do_rest_load_data();
		echo json_encode($response);
	}

}

/* End of file Controler */
/* Generate by TaraCode */