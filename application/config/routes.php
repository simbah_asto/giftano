<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$route['default_controller'] = 'dashboard/admin/index';
$route['m/(:any)'] = '$1/kasir/index';
$route['m/(:any)/(:any)'] = '$1/kasir/$2';


$route['(:any)/(:any)/(:any)'] = '$2/$1/$3';
$route['(:any)/(:any)'] = '$2/$1/index';
$route['(:any)'] = 'www/$1/index';
// $route['']
$route['register'] = 'www/register/index';
$route['login'] = 'www/login/index';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
