<form class="modal-content" id="form-add">
	<div class="modal-header">
		Add kategori
	</div>
	<div class="modal-body">
		<div class="form-group">
			<label>Kategori name</label>
			<input type="text" class="form-control" name="kategori" placeholder="kategori">
		</div>
	</div>
	<div class="modal-footer">
		<input type="submit" class="btn btn-primary" name="" value="Save">
	</div>
</form>


<script type="text/javascript">
	$('#form-add').submit(function(event) {
		event.preventDefault();
		$.ajax({
			url: '<?php echo base_url('admin/kategori/set_kategori') ?>',
			type: 'POST',
			dataType: 'JSON',
			data: $(this).serialize(),
			success:function (data) {
				if (data.status == 200) {
					Notifier.success(data.message, 'Success');
					$('#myModal').modal('hide');
					$('#dataTable').DataTable().ajax.reload();
				}else{
					Notifier.success(data.message, 'Error');
				}
			}, 
			error:function (data) {
				console.log(data);
			}
		})
	});
</script>