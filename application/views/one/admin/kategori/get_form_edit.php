<form class="modal-content" id="form-add">
	<div class="modal-header">
		Add kategori
	</div>
	<?php 
	$data = (isset($response['data']))?$response['data']:array();
	?>
	<?php if (!empty($data)) {?>

		<div class="modal-body">
			<input type="hidden" name="id" value="<?php echo $data['id'] ?>">
			<div class="form-group">
				<label>Kategori name</label>
				<input type="text" class="form-control" name="kategori" placeholder="kategori" value="<?php echo $data['name'] ?>">
			</div>
		</div>
		<div class="modal-footer">
			<input type="submit" class="btn btn-primary" name="" value="Save">
		</div>
	<?php }else{ ?>
		<div class="alert  alert-danger">Your data is empty</div>
	<?php } ?>
</form>


<script type="text/javascript">
	$('#form-add').submit(function(event) {
		event.preventDefault();
		$.ajax({
			url: '<?php echo base_url('admin/kategori/update') ?>',
			type: 'POST',
			dataType: 'JSON',
			data: $(this).serialize(),
			success:function (data) {
				if (data.status == 200) {
					Notifier.success(data.message, 'Success');
					$('#myModal').modal('hide');
					$('#dataTable').DataTable().ajax.reload();
				}else{
					Notifier.success(data.message, 'Error');
				}
			}, 
			error:function (data) {
				console.log(data);
			}
		})
	});
</script>