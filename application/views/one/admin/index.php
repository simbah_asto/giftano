<!DOCTYPE html>
<html>
<head>
	<title>Giftano</title>

	<!-- dashboard -->
	<!-- <link rel="stylesheet" type="text/css" href="../addons/user/js/dashboard.js"> -->
	<!-- /dashboard -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

	<link rel="stylesheet" type="text/css" href="<?php echo base_url('addons/plugins/css/dashboard.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('addons/css/main_user.css') ?>">
	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('addons/css/mainAdministrator.css') ?>">
	<link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">

	<link rel="stylesheet" type="text/css" href="<?php echo base_url('addons/dataTables/dataTables.bootstrap4.min.css') ?>">

</head>

<body>

	<div class="page">
		<div class="page-main">
			<!-- navbar -->
			<?php $this->load->view($navbar) ?>
			<!-- navbar -->
			<?php $this->load->view($view) ?>

		</div>
	</div>

	<input type="hidden" name="url" id="url" value="<?php echo base_url() ?>">
	<script type="text/javascript" src="<?php echo base_url('addons/js/mainform.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('addons/notifer/notifer.js') ?>"></script>

	<script type="text/javascript" src="<?php echo base_url('addons/dataTables/jquery.dataTables.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('addons/dataTables/dataTables.bootstrap4.min.js') ?>"></script>

	<script type="text/javascript" src="<?php echo base_url('addons/js/main.js') ?>"></script>

</body>
</html>