<?php 
$kategori = (isset($kategori['data']))?$kategori['data']:array();
$data = (isset($data['data']))?$data['data']:array();
?>

<form class="modal-content" id="form-add">
	<div class="modal-header">
		Add product
	</div>

	<div class="modal-body">
		<input type="hidden" name="id" value="<?php echo $data['id'] ?>">
		<div class="form-group">
			<label>Product name</label>
			<input type="text" class="form-control" name="name" placeholder="product name" value="<?php echo $data['name'] ?>">
		</div>

		<div class="form-group">
			<label>Kategori name</label>
			<select class="form-control" name="kategori">
				<option value="0">Pilih kategori</option>
				<?php if (!empty($kategori)) {?>
					<?php foreach ($kategori as $value) {?>
						<option value="<?php echo $value['id'] ?>" <?php if($value['id'] == $data['kategori_id']) echo "selected" ?>><?php echo $value['name'] ?></option>
					<?php } ?>
				<?php } ?>
			</select>
		</div>

		<div class="form-group">
			<label>Product price</label>
			<input type="number" class="form-control" name="harga" placeholder="price" value="<?php echo $data['produk_price'] ?>">
		</div>

		<div class="form-group">
			<label>Product Deskripsi</label>
			<textarea name="deskripsi" rows="5" class="form-control"><?php echo $data['produk_deskripsi'] ?></textarea>
		</div>

	</div>
	<div class="modal-footer">
		<input type="submit" class="btn btn-primary" name="" value="Save">
	</div>
</form>


<script type="text/javascript">
	$('#form-add').submit(function(event) {
		event.preventDefault();
		$.ajax({
			url: '<?php echo base_url('admin/product/update_produk') ?>',
			type: 'POST',
			dataType: 'JSON',
			data: $(this).serialize(),
			success:function (data) {
				if (data.status == 200) {
					Notifier.success(data.message, 'Success');
					$('#myModal').modal('hide');
					$('#dataTable').DataTable().ajax.reload();
				}else{
					Notifier.success(data.message, 'Error');
				}
			}, 
			error:function (data) {
				console.log(data);
			}
		})
	});
</script>