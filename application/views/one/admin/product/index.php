	<!-- modal -->
	<div class="row w-100">
		<div class="col-12">
			<div class="modal fade" id="myModal">
				<div class="modal-dialog">
					<!-- data page -->
				</div>
			</div>
		</div>
	</div>
	<!-- modal -->

	<div class="my-3 my-md-5">
		<div class="container">
			<div class="page-header d-sm-flex align-items-center justify-content-between">
				<h1 class="page-title">
					Product
				</h1>
				<!-- button -->
				<button id="add" value="product/get_form_input" class="d-none float-right d-sm-inline-block btn btn-success shadow-sm">Tambah</button>
				<!-- button -->
			</div>
			
		</div>
	</div>


	<div class="container">
		<div class="table-responsive">
			<table id="dataTable" class="w-100 table table-bordered table-hover">
				<thead class="bg-white">
					<th width="5%" class="">No</th>
					<th>Product</th>
					<th>Price</th>
					<th>Kategori</th>
					<th>Datetime</th>
					<th width="10%">Option</th>
				</thead>
				<tbody >

				</tbody>
			</table>
		</div>


		<script type="text/javascript">
			$(document).ready(function() {
				onload_data();
			});

			function onload_data () {
				$('#dataTable').DataTable({
					ajax: {
						url: '<?php echo base_url('admin/product/rest_load_data') ?>',
						dataSrc: 'data'
					},
					lengthChange: true,
					searching:true,
					"bInfo": true,
					"bLengthChange": true,
					"bPaginate": true,
					"columns":[

					{"data":"no"},
					{"data":"produk"},
					{"data":"harga"},
					{"data":"kategori"},

					{"data":"datetime"},
					{
						"data":null, 
						"bSortable":false, 
						"mRender":function (o) {
							return button(o.id);
						}
					},
					]
				})
			}

			function button(id) {
				return '<button class="edit btn btn-sm btn-warning mr-1" value="" onclick="do_edit('+id+')"><i class="fas fa-pen"></i></button>'+'<button class="deleted btn btn-sm btn-danger" onclick="do_deleted('+id+')" value="1"><i class="far fa-trash-alt"></i></button>';
			}

			function do_deleted(id) {
				$.ajax({
					url: '<?php echo base_url('admin/product/deleted') ?>',
					type: 'POST',
					dataType: 'JSON',
					data: {id: id},
					success:function (data) {
						if (data.status == 200) {
							Notifier.success(data.message, 'Success');
							$('#dataTable').DataTable().ajax.reload();
						} else {
							Notifier.success(data.message, 'Error');
						}
					},
					error:function (data) {
						console.log(data);
					}
				})
			}

			function do_edit(id) {
				$.ajax({
					url: '<?php echo base_url('admin/product/get_form_edit') ?>',
					type: 'POST',
					dataType: 'html',
					data: {id: id},
					success:function (data) {
						$('#myModal').modal('show');
						$('.modal-dialog').html(data);
					},
					error:function (data) {
					}
				})
			}

		</script>