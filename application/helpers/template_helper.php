<?php
defined('BASEPATH') OR exit('No direct script access allowed');


function template($model= '', $page = '', $data = array(), $view = '')
{
	$CI =& get_instance();
	$data['view'] = $CI->config->item('core_template').'/'.$model.'/'.$page.'/'.$view;
	$CI->load->view($CI->config->item('core_template').'/'.$model.'/index', $data);
}

/* End of file Template_helper.php */
/* Location: ./application/helpers/Template_helper.php */
