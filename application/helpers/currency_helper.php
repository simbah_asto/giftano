<?php
defined('BASEPATH') OR exit('No direct script access allowed');


function currency_rupiah($value)

{

	$result=number_format($value,2,',','.');

	return 'Rp. '.$result;

}

/* End of file currency_helper.php */
/* Location: ./application/helpers/currency_helper.php */