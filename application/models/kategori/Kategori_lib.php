<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Build by generator taraCode
 *
 *
 * @package Models class
 * @author Asto
 */

class Kategori_lib
{
	protected $ci;
	protected $table='no table';

	public function __construct()
	{
		$this->ci =& get_instance();
	}

	// insert data function
	public function do_insert_data(){
		$dataPost = (isset($_POST))?$_POST:array();
		$message = "Data cannot be empty";
		$status = 500;
		$datetime = date('Y/m/d H:i:s');

		if (!empty($dataPost)) {
			$where = array('kategori_name' => $dataPost['kategori'], 'kategori_was_deleted' => 'N');
			$ceking = $this->do_ceking($where);
			if (!empty($ceking)) {
				return array('message' => 'kategori is alerady exsits!', 'status' => $status);
			}

			if (strlen(trim($dataPost['kategori'])) > 100) {
				return array('message' => 'kategori max 100 charakter', 'status' => $status);
			}

			$data = array('kategori_name' => $dataPost['kategori'], 'kategori_datetime' => $datetime);
			$response = $this->ci->base->insert_data_lib('sys_kategori', $data);
			if ($response['status'] == 200) {
				return array('message' => 'kategori save is success', 'status' => 200);
			}

			return array('message' => 'kategori save is failed, error database', 'status' => $status );
		}
		return array('message' => $message, 'status' => $status);

	}
	// load data function
	public function do_rest_load_data(){
		$result = array('pageIndex' => '', 'countItems' => '', 'data' => array());
		$where = array('kategori_was_deleted' => 'N');
		$data = $this->do_ceking($where);
		if (!empty($data)) {
			$i = 1;
			foreach ($data as $value) {
				$result['data'][] = array('name' => $value['name'], 'id' => $value['id'], 'no' => $i,'datetime' => convert_datetime($value['datetime']));
				$i++;
			}
		}

		return $result;
	}
	// update data function
	public function do_updated_data(){
		$dataPost = (isset($_POST))?$_POST:array();
		$message = 'Data cannot me empty';
		$status = 500;

		if (!empty($dataPost)) {
			$where = array('kategori_id !=' => $dataPost['id'], 'kategori_name' => $dataPost['kategori'], 'kategori_was_deleted' => 'N');
			$ceking = $this->do_ceking($where);
			if (!empty($ceking)) {
				return array('message' => 'kategori is alerady exsits!', 'status' => $status);
			}

			if (strlen(trim($dataPost['kategori'])) > 100) {
				return array('message' => 'kategori max 100 charakter', 'status' => $status);
			}

			if (!intval($dataPost['id'])) {
				return array('message' => 'Your id is not valid', 'status' => $status, 'data' => $data);
			}

			$udpate_where = array('kategori_id' => $dataPost['id'], 'kategori_was_deleted' => 'N');

			$update_val = array('kategori_name' => $dataPost['kategori']);
			$response = $this->ci->base->update_data_lib('sys_kategori', $update_val, $udpate_where);

			if ($response['status'] == 200) {
				return array('message' => $dataPost['kategori'].' kategori is updated', 'status' => 200);
			}

			$message = 'Updated kategori is failed';
			
		}

		return array('message' => $message, 'status' => $status, 'data' => $data);
	}
	// deleted data function
	public function do_deleted_data(){
		$dataPost = (isset($_POST))?$_POST:array();
		$message = 'Data cannot be empty';
		$status = 500;
		$datetime = date('Y/m/d H:i:s');
		$data = array();

		if (!empty($dataPost)) {
			if (!intval($dataPost['id']) OR $dataPost['id'] < 1) {
				return array('message' => 'your id produk is not valid', 'status' => $status);
			}

			$data = array('kategori_was_deleted' => 'Y');
			$where = array('kategori_id' => $dataPost['id'], 'kategori_was_deleted' => 'N');
			$response = $this->ci->base->update_data_lib('sys_kategori', $data, $where);
			if ($response['status'] == 200) {
				return array('message' => 'product deleted is success', 'status' => 200);
			}

			$message = "Fail your deleted product";
		}

		return array('message' => $message, 'status' => $status, 'data' => $data);
	}
	// get data by id
	public function do_get_data_by_id(){
		$dataPost = (isset($_POST))?$_POST:array();
		$message = 'Data cannot me empty';
		$status = 500;
		$data = array();
		if (!empty($dataPost)) {
			if (!intval($dataPost['id'])) {
				return array('message' => 'Your id is not valid', 'status' => $status, 'data' => $data);
			}

			$where = array('kategori_was_deleted' => 'N', 'kategori_id' => $dataPost['id']);
			$data = $this->do_ceking($where);
			if (!empty($data)) {
				return array('message' => 'success your get data', 'status' => 200, 'data' => $data[0]);
			}

			$message = 'Kategori is not found';
		}

		return array('message' => $message, 'status' => $status, 'data' => $data);
	}

	public function do_ceking($where = array())
	{
		$result = array();
		if (!empty($where)) {
			$column = array('kategori_id as id', 'kategori_datetime as datetime', 'kategori_name as name');
			$result = $this->ci->base->load_data_lib('sys_kategori', $column, $where);
		}

		return $result;
	}

}

/* End of file Model.php */
/* Location: .//C/wamp64/www/test/cmsGenerator/asGen/core/Model.php */