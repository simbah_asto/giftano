<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Build by generator taraCode
 *
 *
 * @package Models class
 * @author Asto
 */

class Product_lib
{
	protected $ci;
	protected $table='no table';

	public function __construct()
	{
		$this->ci =& get_instance();
	}

	// insert data function
	public function do_insert_data(){

		$dataPost = (isset($_POST))?$_POST:array();
		$message = 'Data cannot be empty';
		$status = 500;
		$datetime = date('Y/m/d H:i:s');

		if (!empty($dataPost)) {
			if (!intval($dataPost['kategori']) OR $dataPost['kategori'] < 1) {
				return array('message' => 'Your kategori is not valid !', 'status' => $status);
			}

			if (!floatval($dataPost['harga'])) {
				return array('message' => 'Your harga is not valid', 'status' => $status);
			}

			$where = array('produk_name' => $dataPost['name'], 'produk_was_deleted' => 'N');
			$ceking = $this->do_ceking($where);
			if (!empty($ceking)) {
				return array('message' => 'your produk name is exsits', 'status' => $status);
			}

			$data = array('produk_name' => $dataPost['name'], 'produk_deskripsi' => $dataPost['deskripsi'], 'produk_datetime' => $datetime, 'produk_price' => $dataPost['harga'], 'kategori_id' => $dataPost['kategori']);
			$response = $this->ci->base->insert_data_lib('sys_produk', $data);
			if ($response['status'] == 200) {
				return array('message' => 'produk save is success', 'status' => 200);
			}
			$message = 'Product saved is fail';

		}

		return array('message' => $message, 'status' => $status);
	}
	// load data function
	public function do_rest_load_data(){
		$data = array('pageIndex' => 0, 'countItems' => 0, 'data' => array());
		$sql = "SELECT sys_produk.produk_name as name, sys_produk.produk_datetime as datetime, sys_produk.produk_price as harga, sys_kategori.kategori_name as kategori, sys_produk.produk_id as  id FROM sys_produk INNER JOIN sys_kategori ON sys_produk.kategori_id = sys_kategori.kategori_id WHERE sys_produk.produk_was_deleted = 'N'";
		$result = $this->ci->base->sql_data_lib($sql);
		if (!empty($result)) {
			$i = 1;
			foreach ($result as $value) {

				$data['data'][] = array('produk' => ucwords($value['name']), 'kategori' => $value['kategori'], 'datetime' => convert_datetime($value['datetime']), 'harga' => currency_rupiah($value['harga']), 'id' => $value['id'], 'no' => $i);
				$i++;
			}
		}

		return $data;
	}
	// update data function
	public function do_updated_data(){
		$dataPost = (isset($_POST))?$_POST:array();
		$message = 'Data cannot be empty';
		$status = 500;
		$datetime = date('Y/m/d H:i:s');

		if (!empty($dataPost)) {
			if (!intval($dataPost['kategori'])) {
				return array('message' => 'Your kategori is not valid !', 'status' => $status);
			}

			if (!floatval($dataPost['harga'])) {
				return array('message' => 'Your harga is not valid', 'status' => $status);
			}

			if (!intval($dataPost['id']) OR $dataPost['id'] < 1) {
				return array('message' => 'your id produk is not valid', 'status' => $status);
			}

			$where = array('produk_name' => $dataPost['name'], 'produk_was_deleted' => 'N', 'produk_id !=' => $dataPost['id']);
			$ceking = $this->do_ceking($where);
			if (!empty($ceking)) {
				return array('message' => 'your produk name is exsits', 'status' => $status);
			}

			$update_where = array('produk_id' => $dataPost['id'], 'produk_was_deleted' => 'N');
			$udpate_val =  array('produk_name' => $dataPost['name'], 'produk_deskripsi' => $dataPost['deskripsi'], 'produk_datetime' => $datetime, 'produk_price' => $dataPost['harga']);
			$response = $this->ci->base->update_data_lib('sys_produk', $udpate_val, $update_where);
			if ($response['status'] == 200) {
				return array('message' => 'Produk updated is success', 'status' => 200);	
			}

			$message = "Fail your update data";

		}

		return array('message' => $message, 'status' => $status);
	}
	// deleted data function
	public function do_deleted_data(){
		$dataPost = (isset($_POST))?$_POST:array();
		$message = 'Data cannot be empty';
		$status = 500;
		$datetime = date('Y/m/d H:i:s');
		$data = array();

		if (!empty($dataPost)) {
			if (!intval($dataPost['id']) OR $dataPost['id'] < 1) {
				return array('message' => 'your id produk is not valid', 'status' => $status);
			}

			$data = array('produk_was_deleted' => 'Y');
			$where = array('produk_id' => $dataPost['id'], 'produk_was_deleted' => 'N');
			$response = $this->ci->base->update_data_lib('sys_produk', $data, $where);
			if ($response['status'] == 200) {
				return array('message' => 'product deleted is success', 'status' => 200);
			}

			$message = "Fail your deleted product";
		}

		return array('message' => $message, 'status' => $status, 'data' => $data);
	}
	// get data by id
	public function do_get_data_by_id(){
		$dataPost = (isset($_POST))?$_POST:array();
		$message = 'Data cannot be empty';
		$status = 500;
		$datetime = date('Y/m/d H:i:s');
		$data = array();

		if (!empty($dataPost)) {
			if (!intval($dataPost['id']) OR $dataPost['id'] < 1) {
				return array('message' => 'your id produk is not valid', 'status' => $status);
			}

			$where = array('produk_id' => $dataPost['id'], 'produk_was_deleted' => 'N');
			$data = $this->do_ceking($where);
			if (!empty($data)) {
				return array('message' => 'success your get data', 'status' => 200, 'data' => $data[0]);
			}

			$message = 'Your produk is not be find';
		}

		return array('message' => $message, 'status' => $status, 'data' => $data);
	}

	public function do_ceking($where = array())
	{
		$result = array();
		if (!empty($where)) {
			$column = array('produk_name as name', 'produk_deskripsi', 'produk_datetime', 'produk_price', 'kategori_id', 'produk_id as id');
			$result = $this->ci->base->load_data_lib('sys_produk', $column, $where);
		}

		return $result;
	}

}

/* End of file Model.php */
/* Location: .//C/wamp64/www/test/cmsGenerator/asGen/core/Model.php */